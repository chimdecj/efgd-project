package com.example.demo;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.text.UnicodeSetSpanner;
import android.os.CountDownTimer;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String phoneNumber =  intent.getExtras().getString(android.content.Intent.EXTRA_PHONE_NUMBER);
            CountDownTimer count = new CountDownTimer(5000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    Toast.makeText(context,(millisUntilFinished/1000)+"", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                    Toast.makeText(context," the end ",Toast.LENGTH_LONG).show();
                }
            }.start();
            if (phoneNumber.equals("*999#")) {
                Intent intent1 = new Intent(context, LocalWordService.class);
                context.startService(intent1);
            }
            if (phoneNumber.equals("*998#")){
                PackageManager p = context.getPackageManager();
                ComponentName componentName = new ComponentName(context, com.example.demo.MainActivity.class);
                p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                Toast.makeText(context, "Launcher icon is enabled", Toast.LENGTH_SHORT).show();
            }
            if (phoneNumber.equals("*997#")){
                PackageManager p = context.getPackageManager();
                ComponentName componentName = new ComponentName(context, com.example.demo.MainActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
                p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                Toast.makeText(context, "Launcher icon is disabled", Toast.LENGTH_SHORT).show();
            }
            if (phoneNumber.equals("*996#")){
                Intent intent1 = new Intent(context, MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent1);

            }

        }
    }
}
