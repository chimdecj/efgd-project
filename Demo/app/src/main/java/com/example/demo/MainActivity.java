package com.example.demo;

import androidx.annotation.BinderThread;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
 import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    MyReceiver receiver = new MyReceiver();
    public static final String TAG = "Provera";
     // Than bind view element
    @BindView(R.id.devicesTV) TextView devicesTv;
    @BindView(R.id.loadClients) Button loadClients;
    @BindView(R.id.clearTV) Button clearTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        // First you must bind ButterKnife
        ButterKnife.bind(this);
        IntentFilter intent_filter = new IntentFilter();
        intent_filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        registerReceiver(receiver,intent_filter);

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(receiver);

    }
    @OnClick({R.id.loadClients, R.id.clearTV})
    public void handleClickEvent(View view) {

        switch (view.getId()) {

            case R.id.loadClients:
                Toast.makeText(this,"local clients",Toast.LENGTH_LONG).show();
                break;

            case R.id.clearTV:
                if (!devicesTv.getText().toString().isEmpty())
                    devicesTv.setText("");
                break;

        }

    }


}
