package com.example.darkbee;

public class Call {
    //==============================================================================================
    //=                         utsand ireh duudlaga (negj heseg)                                  =
    //=                                                                                            =
    //==============================================================================================
    private int id;
    private String phone_number = null;
    private String date = null;
    private String path =  null;
    private String encoded = null;
    public Call( String path, String phone_number, String date) {
        this.path = path;
        this.phone_number = phone_number;
        this.date = date;
    }
    public Call(int id,String  path,String phone_number,String date){
        this.path = path;
        this.phone_number = phone_number;
        this.date = date;
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public String getPath(){
        return path;
    }
    public String getPhoneNumber(){
        return phone_number;
    }
    public String getDate(){
        return  date;
    }
    public void setId(int id){
        this.id = id;
    }
    public void setPath(String path){
        this.path = path;
    }
    public void setPhoneNumber(String phone_number){
        this.phone_number = phone_number;
    }
    public void setDate(String date){
        this.date = date;
    }
    public void setEncoded(String encoded){this.encoded = encoded;}
    public String getEncoded(){return encoded;}
}
