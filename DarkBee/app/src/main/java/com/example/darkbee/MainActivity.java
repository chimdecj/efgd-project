package com.example.darkbee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    //====================== receiver iig uusgej bna ================================
    private ContactReceiver contact_receiver = new ContactReceiver();
    //====================== database iig uusgej bna ===============================
    private DBHelper database = new DBHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    public void onStart(){
        super.onStart();
        //========================== receiver burtguulj baina =====================================
        IntentFilter intent_filter = new IntentFilter();
        intent_filter.addAction(Intent.EXTRA_PHONE_NUMBER);
        this.registerReceiver(contact_receiver,intent_filter);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        //this.unregisterReceiver(sms_receiver);
        this.unregisterReceiver(contact_receiver);
    }
    public void createUser(View v ){

    }

}
