package com.example.darkbee;

public class User {
    private String name;
    private String registred_number;

    public User(String name){
        this.name = name;
    }
    public User(String name,String registred_number){
        this.name = name;
        this.registred_number = registred_number;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setRegisteredNumber(String registered_number){
        this.registred_number = registred_number;
    }
    public String getName(){
        return name;
    }
    public String getRegistredNumber(){
        return  registred_number;
    }

}
