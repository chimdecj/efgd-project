package com.example.darkbee;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LocalWordService extends Service {
    private static final String TAG = "LocalWordService";
    private final IBinder m_binder = new MyBinder();
    private List<String> result_list = new ArrayList<String>();
    private int counter = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        addResultValues();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        addResultValues();
        return m_binder;
    }

    public class MyBinder extends Binder {
        LocalWordService getService() {
            return LocalWordService.this;
        }
    }

    public List<String> getWordList() {
        return result_list;
    }

    private void addResultValues() {
        Log.d(TAG, "addResultValues: Service is started");

        Toast.makeText(getApplicationContext(), "Toast from service", Toast.LENGTH_LONG).show();

//        Random random = new Random();
//        List<String> input = Arrays.asList("Linux", "Android","iPhone","Windows7" );
//        resultList.add(input.get(random.nextInt(3)) + " " + counter++);
//        if (counter == Integer.MAX_VALUE) {
//            counter = 0;
//        }
    }
}
