package com.example.darkbee;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.darkbee.App.CHANNEL_ID;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class RecorederService extends Service implements MediaRecorder.OnErrorListener{
    private final IBinder m_binder = new ServiceBinder();
    //======================= Audio file iin zam ===========================
    private String audio_save_path_in_device = null;
    //======================= android recorder ====================================
    private MediaRecorder media_recorder ;
    //======================= database ===============================
    private DBHelper database;
    //======================= android recorderiig ajiluulah notfication ====================
    private NotificationManager notification_manager;
    //======================= request permission code ==============================
    public static final int REQUEST_PERMISSION_CODE = 1;
    @Override
    public void onCreate() {
        Toast.makeText(getApplicationContext(), "create service", Toast.LENGTH_LONG).show();
        database = new DBHelper(getApplicationContext());
    }
    //==================================Service uussen uyd=====================================
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Toast.makeText(getApplicationContext(),"start service",Toast.LENGTH_LONG).show();
        //==============================================================================
        //=                         Permissiong shalgaj baina                          =
        //=               Hervee permision taarval Recorder ajillaj ehelne             =
        //==============================================================================
        if(checkPermission()) {
            createNotificationChannel();
            Intent notification_intent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notification_intent, 0);
            new NotificationCompat.Builder(this);
            //====================== Notification uusgej baij ajillah recorder-iig ajiluulj baina ==========================
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Dial")
                    .setContentText("calling ...")
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentIntent(pendingIntent)
                    .build();
            //====================== Foreground service iig ehluulj baina ==============================================
            startForeground(1, notification);
            Date date = new Date();
            SimpleDateFormat simple_date_format=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            //========================== audio file aa hadgalahd storageDirectory + phone number + datetime
            audio_save_path_in_device = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +ContactReceiver.phone_number+"%"+ date.toString() + ".3gp";
            Call call = new Call(audio_save_path_in_device, ContactReceiver.phone_number,simple_date_format.format(date));
            //========================== Database-d hadgalaj bn ==================================
            database.addCall(call);
            //========================= Recorder iig ajilulj baian =======================================
            mediaRecorderReady();
            try {
                media_recorder.prepare();
                media_recorder.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }
        return START_STICKY;
    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
             notification_manager = getSystemService(NotificationManager.class);
            notification_manager.createNotificationChannel(serviceChannel);
        }
    }
//    private void closeNotificationChannel(){
//        notification_manager.cancel(1);
//    }

    //============================== service-iig ustgaad Recorder iig zogsooj baina =========================================
    @Override
    public void onDestroy() {
        //
        notification_manager.cancelAll();
        media_recorder.stop();
        stopSelf();
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "Recording Completed",Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(),"stop service",Toast.LENGTH_LONG).show();

//        Toast.makeText(getApplicationContext(),"path:"+audioSavePathInDevice+"\n phone_nuber:"+CallReceiver.phone_number,Toast.LENGTH_LONG).show();
    }

    @Override
    public IBinder onBind(Intent intent) { return m_binder; }

    //================================Recorder uusgej baina ==============================================
    public void mediaRecorderReady(){
        media_recorder=new MediaRecorder();
        media_recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        media_recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        media_recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
//        mediaRecorder.setInputSurface(MediaRecorder.OutputFormat.);
        media_recorder.setOutputFile(audio_save_path_in_device);
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions((Activity) getApplicationContext(), new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, REQUEST_PERMISSION_CODE);
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onError(MediaRecorder media_recorder, int i, int i1) {

        Toast.makeText(this, "Music player failed", Toast.LENGTH_SHORT).show();
        if (media_recorder != null) {
            try {
                media_recorder.stop();
                media_recorder.release();
            } finally {
                media_recorder = null;
            }
        }
    }

    public class ServiceBinder extends Binder {
        RecorederService getService() {
            return RecorederService.this;
        }
    }
}
