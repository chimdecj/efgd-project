package com.example.darkbee;

public class SMS {
    //==============================================================================================
    //=                         utsand ireh sms (negj heseg)                                       =
    //=                                                                                            =
    //==============================================================================================
    private int id;
    private String phone_number;
    private String date;
    private String text;

    public SMS( String text, String phone_number, String date) {
        this.text = text;
        this.phone_number = phone_number;
        this.date = date;
    }
    public SMS(int id,String text,String phone_number,String date){
        this.text = text;
        this.phone_number = phone_number;
        this.date = date;
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public String getText(){
        return text;
    }
    public String getPhoneNumber(){
        return phone_number;
    }
    public String getDate(){
        return  date;
    }
    public void setId(int id){
        this.id = id;
    }
    public void setText(String text){
        this.text = text;
    }
    public void setPhoneNumber(String phone_number){
        this.phone_number = phone_number;
    }
    public void setDate(String date){
        this.date = date;
    }
}
